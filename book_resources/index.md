[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/geoocean%2Fcourses%2Fcomic/HEAD?urlpath=tree/)

# Computación en Ingeniería Civil - M2090  

Máster Universitario en Ingenieria de Caminos, Canales y Puertos
Universidad de Cantabria

Fernando J. Méndez Incera (fernando.mendez@unican.es)\
Alba Ricondo Cueva (alba.ricondo@unican.es)

## Contenido
**Estadística Computacional:**
* Series temporales
* Teoría de extremos
* Análisis multivariado de datos
* Geoestadística
* Infraestructura de datos espaciales 
* Técnicas de reducción dimensional de datos (PCA). Teledetección y análisis de imágenes
* Interpolación / Krigging

**Optimización:**
* Optimización lineal
* Optimización no lineal
* Optimización dinámica
* Algoritmos genéticos
* Algoritmos heurísticos evolutivos
* Optimización multiobjetivo
* Modelado matemático con optimización

## Descripción de las bases de datos
Oleaje - Boya Exterior de Bilbao-Vicaya
Puertos del Estado [SWAN](https://www.puertos.es/es-es/oceanografia/Paginas/portus.aspx) (Puertos del Estado)
![buoy](buoy.png)


